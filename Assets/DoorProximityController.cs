﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorProximityController : MonoBehaviour
{
    [SerializeField] private Animator doorAnim = null;

    private bool isOpen = false;


    // public string CheckDistanceTargetName = "Demo_FPSController";
    public float CheckDistance = 4.0f;
    private GameObject Target;

    private void Start()
    {
        doorAnim.SetTrigger("Close");
    }

    private void Update()
    {
        if (Target == null)
            Target = Camera.main.gameObject;

        if (Target == null)
            return;

        if (Vector3.Distance(Target.transform.position, transform.position) < CheckDistance)
        {
            if (!isOpen) // if closed, open the door. Otherwise do jack.
            {
                doorAnim.SetTrigger("Open");
                isOpen = true;
            }
        }
        else
        {
            if (isOpen) // if open, close the door. Then to jack.
            {
                doorAnim.SetTrigger("Close");
                isOpen = false;
            }
        }

    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, CheckDistance);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0.3f, 0, 0.5f);
        Gizmos.DrawSphere(transform.position, CheckDistance);
    }
}
