﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ColorSampler : MonoBehaviour
{
    [SerializeField] Texture2D screenTexture = null;
    public Light[] lights = null;

    private void Start()
    {
        InvokeRepeating("CheckColor", 1.0f, 1.0f);
    }

    void CheckColor()
    {
        var dctColorIncidence = new Dictionary<Color, int>();
        for (int row = 0; row < screenTexture.width; row++)
        {
            for (int col = 0; col < screenTexture.height; col++)
            {
                var pixelColor = screenTexture.GetPixel(row, col);

                if (dctColorIncidence.ContainsKey(pixelColor))
                {
                    dctColorIncidence[pixelColor]++;
                }
                else
                {
                    dctColorIncidence.Add(pixelColor, 1);
                }
            }
        }
        var dictSortByValue = dctColorIncidence.OrderByDescending(x => x.Value).ToDictionary(x => x.Value, x => x.Key);
        for(int i = 0; i <lights.Length; i++)
        {
            lights[i].color = dictSortByValue[0];
        }
    }
}

