﻿using UnityEngine;

public class DoorController : MonoBehaviour {

    [SerializeField] private Animator LeftAnim;
    [SerializeField] private Animator RightAnim; 

    private bool bRightIsOpen = false;
    private bool bLeftIsOpen = false;

    private void Start()
    {
        LeftAnim.SetTrigger("Close");
        RightAnim.SetTrigger("Close");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            ToggleLeftDoor();
        }
        
        if (Input.GetKeyDown(KeyCode.P))
        {
            ToggleRightDoor();
        }
    }

    private void ToggleRightDoor()
    {
        RightAnim.SetTrigger(bRightIsOpen ? "Close" : "Open");
        bRightIsOpen = !bRightIsOpen;
    }

    private void ToggleLeftDoor()
    {
        LeftAnim.SetTrigger(bLeftIsOpen ? "Close" : "Open");
        bLeftIsOpen = !bLeftIsOpen;
    }

}
