﻿using UnityEngine;

public class SlidingScreenController : MonoBehaviour
{
    private Animator anim;
    private bool isOut = false;

    private void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetTrigger("Close");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            ToggleShutterSystem();
        }
    }

    private void ToggleShutterSystem()
    {
        anim.SetTrigger(isOut ? "Close" : "Open");
        isOut = !isOut;
    }
}
