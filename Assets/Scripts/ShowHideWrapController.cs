﻿using UnityEngine;

public class ShowHideWrapController : MonoBehaviour
{
    public GameObject wrapController = null;

    private void Awake()
    {
        wrapController.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            if (wrapController.activeInHierarchy)
            {
                wrapController.SetActive(false);
            }
            else
            {
                wrapController.SetActive(true);
            }
        }
    }
}
