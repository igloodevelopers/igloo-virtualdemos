﻿using UnityEngine.UI;
using UnityEngine;

public class Settings : MonoBehaviour
{
    [SerializeField] private Canvas settingsCanvas = null;
    [SerializeField] private Text wrapAdjustmentText = null;
    [SerializeField] private Text fovAdjustmentText = null;
    [SerializeField] private Material iglooMovieMat = null;

    private void Start()
    {
        settingsCanvas.enabled = false;
        AdjustWrapPosition(0);
        AdjustFOV(60);
    }

    private void Update()
    {
        // Show / Hide settings menu
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            settingsCanvas.enabled = !settingsCanvas.enabled;
        }
    }

    public void AdjustWrapPosition(float val)
    {
        var offsetv2 = new Vector2(val, 0);
        iglooMovieMat.SetTextureOffset("_UnlitColorMap", offsetv2);
        iglooMovieMat.SetTextureOffset("_EmissiveColorMap", offsetv2);
        wrapAdjustmentText.text = $"Wrap Adjustment : {string.Format("{0:0.0#}", val).ToString()}";
    }

    public void AdjustFOV(float val)
    {
        val = Mathf.Clamp((int)val, 1, 179);
        Camera.main.fieldOfView = val;
        fovAdjustmentText.text = $"Field of View : {string.Format("{0:0.0#}", val).ToString()}";
    }
}
