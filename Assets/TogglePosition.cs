﻿using UnityEngine;

public class TogglePosition : MonoBehaviour
{
    public Vector3 position1, position2;

    public void MovePosition()
    {
        if(this.transform.localPosition == position1)
        {
            this.transform.localPosition = position2;

        }
        else
        {
            this.transform.localPosition = position1;
        }
    }
}
