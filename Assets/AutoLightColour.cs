﻿using UnityEngine.Rendering.HighDefinition;
using UnityEngine;
using System.Collections;

public class AutoLightColour : MonoBehaviour
{
    [SerializeField] RenderTexture ScreenTexture = null;

    Vector2[] pixelUV = null;
    Light[] lights = null;
    [SerializeField] private int reductionMultiplier = 10;
    [SerializeField] private float fps = 0.02f;
    private Texture2D tex = null;
    RenderTexture temp = null;
    [SerializeField] private int intensity = 400;
    public bool bSingleTestMode = false;

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        if (!isActiveAndEnabled) return;
        lights = GetComponentsInChildren<Light>();
        pixelUV = new Vector2[lights.Length];

        for(int i = 0; i < lights.Length; i++)
        {
            if (Physics.Raycast(new Ray(lights[i].transform.position, -lights[i].transform.forward), out RaycastHit hit))
            {
                if (!hit.transform.GetComponent<Renderer>()) { return; }
                MeshCollider col = hit.transform.GetComponent<MeshCollider>();

                if (ScreenTexture == null) { Debug.Log("Texture not assigned"); return; }
                pixelUV[i] = hit.textureCoord;
                pixelUV[i].x *= (ScreenTexture.width / reductionMultiplier);
                pixelUV[i].y *= (ScreenTexture.height / reductionMultiplier);
            }
        }
        tex = new Texture2D(ScreenTexture.width / reductionMultiplier, ScreenTexture.height / reductionMultiplier, TextureFormat.RGB24, false);
        temp = new RenderTexture(ScreenTexture.width / reductionMultiplier, ScreenTexture.height / reductionMultiplier, 0);
        if (lights.Length != 0)
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].GetComponent<HDAdditionalLightData>().intensity = intensity;
            }
        if (bSingleTestMode)
        {
            CheckCurrentColourBehindLight();
            return;
        }
        else
        {
            InvokeRepeating("CheckCurrentColourBehindLight", fps, fps);
        }
    }

    public void ToggleScreenLightEffect(bool isActive)
    {
        if (!isActive)
        {
            CancelInvoke("CheckCurrentColourBehindLight");
        }
        else
        {
            Init();
        }
        if(lights != null)
            for(int i = 0; i < lights.Length; i++)
            {
                lights[i].GetComponent<HDAdditionalLightData>().intensity = isActive ? intensity : 0;
            }
    }

    private void CheckCurrentColourBehindLight()
    {
        Graphics.Blit(ScreenTexture, temp);
        RenderTexture.active = temp;
        tex.ReadPixels(new Rect(0, 0, temp.width, temp.height), 0, 0);
        tex.Apply();
        RenderTexture.active = null;
        for (int i = 0; i < lights.Length; i++)
        {
            StartCoroutine(ChangeColourOverTime(fps, lights[i], tex.GetPixel((int)pixelUV[i].x, (int)(tex.width - pixelUV[i].y))));
        }
    }

    internal IEnumerator ChangeColourOverTime(float endTime, Light thisLight, Color newCol)
    {
        Color curCol = thisLight.color;
        float ElapsedTime = 0.0f;
        while(ElapsedTime < endTime)
        {
            ElapsedTime += Time.deltaTime;
            thisLight.color = Color.Lerp(curCol, newCol, ElapsedTime / endTime);
            yield return null;
        }
    }


    private void OnDisable()
    {
        CancelInvoke("CheckCurrentColourBehindLight");
        tex = null;
        temp = null;
    }
}
